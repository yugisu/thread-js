import callWebApi from 'src/helpers/webApiHelper';

export const updateProfile = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/profile',
        type: 'POST',
        request,
    });
    return response.json();
};
