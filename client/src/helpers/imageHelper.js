const DEFAULT_USER_AVATAR = 'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png';

export const getUserImgLink = image => (image
    ? image.link
    : DEFAULT_USER_AVATAR);

export const getAvatarLink = imageLink => (imageLink || DEFAULT_USER_AVATAR);
