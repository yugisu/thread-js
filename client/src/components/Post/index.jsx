import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Button, Form } from 'semantic-ui-react';
import moment from 'moment';

import { getAvatarLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Post = ({
    post, reactPost, updatePostDetails, deletePost, toggleExpandedPost, sharePost, restorePost
}) => {
    const {
        id,
        image,
        body,
        user,
        commentCount,
        createdAt,
        deletedAt,
    } = post;
    let { likers, dislikers } = post;

    likers = likers || [];
    dislikers = dislikers || [];

    const date = moment(createdAt).fromNow();
    const { userId, updatePost } = updatePostDetails;

    const [editing, setEditing] = React.useState(false);
    const [newBody, setBody] = React.useState(body);

    const onBodySubmit = () => {
        if (newBody.length) {
            if (newBody !== body) { updatePost(id, newBody); }
            setEditing(false);
        }
    };

    const isDeleted = deletedAt !== null;

    if (isDeleted) {
        return (
            <Card className={`${styles.post} ${styles.postDeleted}`}>
                {image && <Image src={image.link} wrapped ui={false} />}
                <Card.Content>
                    <Card.Meta>
                        <span className="date">
                        posted by
                            {' '}
                            {user.username}
                            {' - '}
                            {date}
                        </span>
                    </Card.Meta>
                    <Card.Description>{body}</Card.Description>
                </Card.Content>
                <Label
                    as="button"
                    attached="bottom right"
                    onClick={() => { restorePost(id); }}
                >
                    Restore post
                </Label>
            </Card>
        );
    }

    return (
        <Card className={styles.post}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by
                        {' '}
                        {user.username}
                        {' - '}
                        {date}
                    </span>
                    {userId === user.id && (
                        <Button
                            icon="pencil alternate"
                            onClick={() => setEditing(!editing)}
                            basic={!editing}
                            color={editing ? 'blue' : null}
                            compact
                            circular
                            active={editing}
                            size="mini"
                            floated="right"
                        />
                    )}
                </Card.Meta>
                <Card.Description>
                    {editing
                        ? (
                            <Form onSubmit={onBodySubmit} style={{ marginTop: '1rem' }}>
                                <Form.TextArea
                                    name="body"
                                    value={newBody}
                                    placeholder="Do u really update your posts haha?"
                                    onChange={ev => setBody(ev.target.value)}
                                />
                                <Button
                                    icon="trash alternate outline"
                                    color="google plus"
                                    onClick={
                                        (ev) => {
                                            ev.preventDefault();
                                            deletePost(id);
                                            setEditing(false);
                                        }
                                    }
                                    compact
                                />
                                <Button content="Update post" icon="check" color="blue" type="submit" labelPosition="left" compact />
                            </Form>
                        )
                        : body
                    }
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => reactPost(id, true)}>
                    <Icon name="thumbs up" />
                    {likers.length}
                </Label>
                {likers
                    .slice(0, 5)
                    .map(l => (
                        <span className={styles.liker} key={`like-${id}-${l.username}`}>
                            <Image src={getAvatarLink(l.imageLink)} avatar size="mini" />
                            <Label pointing>{l.username}</Label>
                        </span>
                    ))}
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => reactPost(id, false)}>
                    <Icon name="thumbs down" />
                    {dislikers.length}
                </Label>
                {dislikers
                    .slice(0, 5)
                    .map(l => (
                        <span className={styles.disliker} key={`dislike-${id}-${l.username}`}>
                            <Image src={getAvatarLink(l.imageLink)} avatar size="mini" />
                            <Label pointing>{l.username}</Label>
                        </span>
                    ))}
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
                    <Icon name="share alternate" />
                </Label>
            </Card.Content>
        </Card>
    );
};


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    reactPost: PropTypes.func.isRequired,
    updatePostDetails: PropTypes.shape({
        userId: PropTypes.string.isRequired,
        updatePost: PropTypes.func.isRequired
    }).isRequired,
    deletePost: PropTypes.func.isRequired,
    restorePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired
};

export default Post;
