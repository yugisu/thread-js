import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Form, Button, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = (props) => {
    const {
        comment: { body, user, id, createdAt, deletedAt },
        userId,
        onUpdate,
        onDelete,
        onRestore
    } = props;
    const date = moment(createdAt).fromNow();

    const [editing, setEditing] = React.useState(false);
    const [newBody, setBody] = React.useState(body);

    const onBodySubmit = (e) => {
        e.preventDefault();

        if (newBody.length) {
            if (newBody !== body) { onUpdate(id, newBody); }
            setEditing(false);
        }
    };

    const isDeleted = deletedAt !== null;

    if (isDeleted) {
        return (
            <CommentUI className={`${styles.comment} ${styles.commentDeleted}`}>
                <CommentUI.Avatar src={getUserImgLink(user.image)} />
                <CommentUI.Content>
                    <CommentUI.Author as="a">{user.username}</CommentUI.Author>
                    <CommentUI.Metadata>{date}</CommentUI.Metadata>
                    <CommentUI.Text>{body}</CommentUI.Text>
                </CommentUI.Content>
                <Label
                    as="button"
                    attached="bottom right"
                    onClick={() => { onRestore(id); }}
                >
                    Restore comment
                </Label>
            </CommentUI>
        );
    }

    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                    {userId === user.id && (
                        <Button
                            icon="pencil alternate"
                            onClick={() => setEditing(!editing)}
                            basic={!editing}
                            color={editing ? 'blue' : null}
                            compact
                            circular
                            active={editing}
                            size="mini"
                            style={{ marginLeft: '.5rem' }}
                        />
                    )}
                </CommentUI.Metadata>
                <CommentUI.Text>
                    { editing ? (
                        <Form onSubmit={onBodySubmit} style={{ marginTop: '1rem' }}>
                            <Form.TextArea
                                name="body"
                                value={newBody}
                                placeholder="Do u really update your posts haha?"
                                onChange={ev => setBody(ev.target.value)}
                            />
                            <Button
                                icon="trash alternate outline"
                                color="google plus"
                                onClick={
                                    (ev) => {
                                        ev.preventDefault();
                                        onDelete(id);
                                        setEditing(false);
                                    }
                                }
                                compact
                            />
                            <Button content="Update post" icon="check" color="blue" type="submit" labelPosition="left" compact />
                        </Form>
                    ) : body}
                </CommentUI.Text>
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    userId: PropTypes.string.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onRestore: PropTypes.func.isRequired,
};

export default Comment;
