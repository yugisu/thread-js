import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
    reactPost,
    toggleExpandedPost,
    addComment,
    updateComment,
    deleteComment,
    restoreComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

class ExpandedPost extends React.Component {
    state = {
        open: true
    };

    closeModal = () => {
        this.props.toggleExpandedPost();
    }

    render() {
        const {
            post,
            sharePost,
            updatePostDetails,
            deletePost,
            restorePost,
            ...props
        } = this.props;

        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>
                            <Post
                                post={post}
                                reactPost={props.reactPost}
                                updatePostDetails={updatePostDetails}
                                deletePost={id => deletePost(id)}
                                restorePost={restorePost}
                                toggleExpandedPost={props.toggleExpandedPost}
                                sharePost={sharePost}
                            />
                            <CommentUI.Group style={{ maxWidth: '100%' }}>
                                <Header as="h3" dividing>
                                    Comments
                                </Header>
                                {post.comments && post.comments
                                    .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                                    .map(comment => (
                                        <Comment
                                            comment={comment}
                                            onUpdate={this.props.updateComment}
                                            onDelete={this.props.deleteComment}
                                            onRestore={this.props.restoreComment}
                                            userId={this.props.userId}
                                            key={comment.id}
                                        />
                                    ))
                                }
                                <AddComment postId={post.id} addComment={props.addComment} />
                            </CommentUI.Group>
                        </Modal.Content>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

ExpandedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    reactPost: PropTypes.func.isRequired,
    updatePostDetails: PropTypes.shape({
        userId: PropTypes.string.isRequired,
        updatePost: PropTypes.func.isRequired
    }).isRequired,
    deletePost: PropTypes.func.isRequired,
    restorePost: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
    updateComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    restoreComment: PropTypes.func.isRequired,
    userId: PropTypes.string.isRequired,
    sharePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
    post: rootState.posts.expandedPost,
    userId: rootState.profile.user.id
});
const actions = {
    reactPost,
    toggleExpandedPost,
    addComment,
    updateComment,
    deleteComment,
    restoreComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExpandedPost);
