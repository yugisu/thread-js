import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isEmail } from 'validator';

import { getUserImgLink } from 'src/helpers/imageHelper';
import { Grid, Image, Input, Button } from 'semantic-ui-react';
import { updateUser } from './actions';


const useUser = (givenUser) => {
    const [user, setUser] = useState(givenUser);

    const changeStatus = (status) => {
        if (status.length < 40) {
            setUser(u => ({ ...u, status }));
        }
    };

    const changeUsername = (username) => {
        setUser(u => ({ ...u, username }));
    };

    const changeEmail = (email) => {
        setUser(u => ({ ...u, email }));
    };

    return { user, changeStatus, changeUsername, changeEmail };
};

const Profile = (props) => {
    const { user, changeStatus, changeUsername, changeEmail } = useUser(props.user);

    const onUpdateUser = () => {
        if (user) {
            if (isEmail(user.email)) {
                props.updateUser(user);
            }
        }
    };

    return (
        <Grid container textAlign="center" style={{ paddingTop: 30 }}>
            <Grid.Column>
                <Image centered src={getUserImgLink(user.image)} size="medium" circular />
                <br />
                <Input
                    icon="comment outline"
                    iconPosition="left"
                    placeholder="Status"
                    type="text"
                    value={user.status || ''}
                    onChange={e => changeStatus(e.target.value)}
                />
                <br />
                <br />
                <Input
                    icon="user"
                    iconPosition="left"
                    placeholder="Username"
                    type="text"
                    value={user.username}
                    onChange={e => changeUsername(e.target.value)}
                />
                <br />
                <br />
                <Input
                    icon="at"
                    iconPosition="left"
                    placeholder="Email"
                    type="email"
                    value={user.email}
                    onChange={e => changeEmail(e.target.value)}
                />
                <br />
                <br />
                <Button content="Update profile" onClick={onUpdateUser} />
            </Grid.Column>
        </Grid>
    );
};

Profile.propTypes = {
    user: PropTypes.objectOf(PropTypes.any),
    updateUser: PropTypes.func.isRequired,
};

Profile.defaultProps = {
    user: {}
};

const mapStateToProps = rootState => ({
    user: rootState.profile.user
});

const actions = {
    updateUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
