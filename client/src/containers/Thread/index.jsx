import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Loader, Button, Checkbox } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
    loadPosts,
    loadMorePosts,
    reactPost,
    updatePost,
    deletePost,
    restorePost,
    toggleExpandedPost,
    addPost } from './actions';

import styles from './styles.module.scss';

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showSetting: 1,
            showDeleted: false
        };
        this.postsFilter = {
            from: 0,
            count: 10
        };
    }

    togglePosts = (setting = 1) => {
        this.setState(
            { showSetting: setting, showDeleted: false },
            () => {
                const { showSetting } = this.state;
                const { userId } = this.props;

                this.postsFilter = {
                    ...this.postsFilter,
                    from: 0,
                };

                this.props.loadPosts({ ...this.postsFilter, showSetting, userId });
                this.postsFilter.from = this.postsFilter.count;
            }
        );
    };

    loadMorePosts = () => {
        const { showSetting } = this.state;
        const { userId } = this.props;

        this.props.loadMorePosts({ ...this.postsFilter, showSetting, userId });
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    }

    sharePost = (sharedPostId) => {
        this.setState({ sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    }

    toggleShowDeleted = () => {
        this.setState(({ showDeleted }) => ({ showDeleted: !showDeleted }));
    }

    showSettingToggler = (num) => {
        const { showSetting } = this.state;
        return ({
            active: showSetting === num,
            onClick: () => showSetting !== num && this.togglePosts(num)
        });
    }

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { posts = [], expandedPost, hasMorePosts, userId, ...props } = this.props;
        const { showSetting, sharedPostId, showDeleted } = this.state;

        let postsToShow = posts;

        if (!showDeleted) {
            postsToShow = posts.filter(p => !p.deletedAt);
        }

        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost addPost={props.addPost} uploadImage={this.uploadImage} />
                </div>
                <div className={styles.toolbar}>
                    <Button.Group fluid basic>
                        <Button
                            icon="align justify"
                            content="All"
                            {...this.showSettingToggler(1)}
                        />
                        <Button
                            icon="hand peace outline"
                            content="All but own"
                            {...this.showSettingToggler(2)}
                        />
                        <Button
                            icon="user circle outline"
                            content="Own posts"
                            {...this.showSettingToggler(3)}
                        />
                        <Button
                            icon="heart outline"
                            content="Liked"
                            {...this.showSettingToggler(4)}
                        />
                    </Button.Group>
                </div>
                {
                    showSetting === 3 && (
                        <div className={styles.toolbar}>
                            <Checkbox
                                checked={showDeleted}
                                label="Show deleted"
                                toggle
                                onChange={this.toggleShowDeleted}
                            />
                        </div>
                    )
                }
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {postsToShow.map(post => (
                        <Post
                            post={post}
                            reactPost={props.reactPost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            deletePost={props.deletePost}
                            restorePost={props.restorePost}
                            sharePost={this.sharePost}
                            updatePostDetails={{ userId, updatePost: props.updatePost }}
                            key={post.id}
                        />
                    ))}
                </InfiniteScroll>
                {
                    expandedPost && (
                        <ExpandedPost
                            sharePost={this.sharePost}
                            deletePost={this.props.deletePost}
                            restorePost={this.props.restorePost}
                            updatePostDetails={{ userId, updatePost: props.updatePost }}
                        />
                    )
                }
                {
                    sharedPostId
                    && <SharedPostLink postId={sharedPostId} close={this.closeSharePost} />
                }
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    reactPost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    restorePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user.id
});

const actions = {
    loadPosts,
    loadMorePosts,
    reactPost,
    updatePost,
    deletePost,
    restorePost,
    toggleExpandedPost,
    addPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
