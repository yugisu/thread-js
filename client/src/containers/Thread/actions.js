import moment from 'moment';
import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

export const loadPosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts
        .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const reactPost = (postId, liked = true) => async (dispatch, getRootState) => {
    const {
        isLike,
        createdAt,
        updatedAt,
        userId: sentUserId
    } = await postService.reactPost(postId, liked);
    const {
        posts: { posts, expandedPost },
        profile: { user: { username, image, id: userId } }
    } = getRootState();

    let imageLink = null;

    if (image) {
        imageLink = image.link;
    }

    const mapLikes = (post) => {
        let likers = post.likers || [];
        let dislikers = post.dislikers || [];

        const addUser = array => [{ userId, username, imageLink }, ...array];
        const removeUser = (array) => {
            const idx = array.findIndex(l => l.userId === userId);
            if (idx > -1) {
                return [...array.slice(0, idx), ...array.slice(idx + 1)];
            }
            return array;
        };

        if (sentUserId === undefined) {
            if (liked) {
                likers = removeUser(likers);
            } else {
                dislikers = removeUser(dislikers);
            }
        } else if (isLike === true) {
            likers = addUser(likers);
            if (createdAt !== updatedAt) {
                dislikers = removeUser(dislikers);
            }
        } else if (isLike === false) {
            dislikers = addUser(dislikers);
            if (createdAt !== updatedAt) {
                likers = removeUser(likers);
            }
        }

        return {
            ...post,
            likers,
            dislikers
        };
    };

    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const updateComment = (id, body) => async (dispatch, getRootState) => {
    const comment = await commentService.updateComment(id, body);

    const mapComments = post => ({
        ...post,
        comments: (post.comments || []).map(c => (c.id === id ? { ...c, ...comment } : c))
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const deleteComment = id => async (dispatch, getRootState) => {
    await commentService.deleteComment(id);

    const { posts: { posts, expandedPost } } = getRootState();

    let comment;
    if (expandedPost.comments) {
        comment = expandedPost.comments.find(c => c.id === id);
    }

    if (!comment) return;

    const deletedAt = moment().toISOString();

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) - 1,
        comments: (post.comments || []).map(c => (c.id === id ? { ...c, deletedAt } : c))
    });

    const updated = posts.map(post => (post.id === comment.postId ? mapComments(post) : post));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const restoreComment = id => async (dispatch, getRootState) => {
    await commentService.restoreComment(id);

    const { posts: { posts, expandedPost } } = getRootState();

    let comment;
    if (expandedPost.comments) {
        comment = expandedPost.comments.find(c => c.id === id);
    }

    if (!comment) return;

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: (post.comments || []).map(c => (c.id === id ? { ...c, deletedAt: null } : c))
    });

    const updated = posts.map(post => (post.id === comment.postId ? mapComments(post) : post));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const updatePost = (postId, body) => async (dispatch, getRootState) => {
    await postService.updatePost(postId, body);

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : { ...post, body }));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction({ ...expandedPost, body }));
    }
};

export const deletePost = postId => async (dispatch, getRootState) => {
    await postService.deletePost(postId);

    const { posts: { posts, expandedPost } } = getRootState();

    const deletedAt = moment().toISOString();
    const updated = posts.map(post => (post.id === postId ? { ...post, deletedAt } : post));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(null));
    }
};

export const restorePost = postId => async (dispatch, getRootState) => {
    await postService.restorePost(postId);

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id === postId ? { ...post, deletedAt: null } : post));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction({ ...expandedPost, deletedAt: null }));
    }
};
