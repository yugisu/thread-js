import { CommentModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './base.repository';

class CommentRepository extends BaseRepository {
    getCommentById(id) {
        return this.model.findOne({
            group: [
                'comment.id',
                'user.id',
                'user->image.id'
            ],
            where: { id },
            include: [{
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }]
        });
    }

    async deleteById(id, userId) {
        const commentInDB = await this.model.findOne({
            where: { id },
            include: [{
                model: UserModel,
                attributes: ['id']
            }]
        });

        if (commentInDB && commentInDB.user && commentInDB.user.id === userId) {
            return this.model.destroy({ where: { id } });
        }

        return null;
    }

    async restoreCommentById(id) {
        return this.model.restore({ where: { id } });
    }
}

export default new CommentRepository(CommentModel);
