import { Op } from 'sequelize';
import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel } from '../models/index';
import BaseRepository from './base.repository';

class PostRepository extends BaseRepository {
    async getPosts(filter) {
        const {
            from: offset,
            count: limit,
            userId,
            showSetting
        } = filter;

        const settings = {};

        switch (Number(showSetting)) {
        case 1:
            break;
        case 2:
            settings.where = { userId: { [Op.not]: userId } };
            break;
        case 3:
            settings.where = { userId };
            settings.paranoid = false;
            break;
        case 4:
            const postReactions = await PostReactionModel
                .findAll({ where: { userId, isLike: true }, attributes: ['postId'] }) || [];

            settings.where = { id: { [Op.in]: postReactions.map(r => r.postId) } };
            break;
        default:
            break;
        }

        return this.model.findAll({
            ...settings,
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" is NULL)`),
                    'commentCount'],
                    [sequelize.literal(`
                    (SELECT
                        json_agg(json_build_object(
                            'userId', "user"."id",
                            'username', "user"."username",
                            'imageLink', "image"."link"
                        )) as likers
                    FROM
                        "users" as "user"
                        LEFT JOIN "images" as "image" 
                        ON "user"."imageId" = "image"."id"
                        LEFT JOIN "postReactions"
                        ON "user"."id" = "postReactions"."userId"
                    WHERE "post"."id" = "postReactions"."postId" AND "postReactions"."isLike" = 'true')
                    `),
                    'likers'],
                    [sequelize.literal(`
                    (SELECT
                        json_agg(json_build_object(
                            'userId', "user"."id",
                            'username', "user"."username",
                            'imageLink', "image"."link"
                        )) as likers
                    FROM 
                        "users" as "user"
                        LEFT JOIN "images" as "image" 
                        ON "user"."imageId" = "image"."id"
                        LEFT JOIN "postReactions"
                        ON "user"."id" = "postReactions"."userId"
                    WHERE "post"."id" = "postReactions"."postId" AND "postReactions"."isLike" = 'false')
                    `),
                    'dislikers']
                ]
            },
            include: [{
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: PostReactionModel,
                attributes: [],
                duplicating: false
            }],
            group: [
                'post.id',
                'image.id',
                'user.id',
                'user->image.id'
            ],
            order: [['createdAt', 'DESC']],
            offset,
            limit
        });
    }

    getPostById(id) {
        return this.model.findOne({
            group: [
                'post.id',
                'comments.id',
                'comments->user.id',
                'comments->user->image.id',
                'user.id',
                'user->image.id',
                'image.id'
            ],
            where: { id },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" is NULL)`),
                    'commentCount'],
                    [sequelize.literal(`
                    (SELECT
                        json_agg(json_build_object(
                            'userId', "user"."id",
                            'username', "user"."username",
                            'imageLink', "image"."link"
                        )) as likers
                    FROM
                        "users" as "user"
                        LEFT JOIN "images" as "image" 
                        ON "user"."imageId" = "image"."id"
                        LEFT JOIN "postReactions"
                        ON "user"."id" = "postReactions"."userId"
                    WHERE "post"."id" = "postReactions"."postId" AND "postReactions"."isLike" = 'true')
                    `),
                    'likers'],
                    [sequelize.literal(`
                    (SELECT
                        json_agg(json_build_object(
                            'userId', "user"."id",
                            'username', "user"."username",
                            'imageLink', "image"."link"
                        )) as likers
                    FROM 
                        "users" as "user"
                        LEFT JOIN "images" as "image" 
                        ON "user"."imageId" = "image"."id"
                        LEFT JOIN "postReactions"
                        ON "user"."id" = "postReactions"."userId"
                    WHERE "post"."id" = "postReactions"."postId" AND "postReactions"."isLike" = 'false')
                    `),
                    'dislikers']
                ]
            },
            include: [{
                model: CommentModel,
                include: {
                    model: UserModel,
                    attributes: ['id', 'username'],
                    include: {
                        model: ImageModel,
                        attributes: ['id', 'link']
                    }
                },
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: PostReactionModel,
                attributes: []
            }]
        });
    }

    async deleteById(id, userId) {
        const postInDB = await this.model.findOne({
            where: { id },
            include: [{
                model: UserModel,
                attributes: ['id']
            }]
        });

        if (postInDB && postInDB.user && postInDB.user.id === userId) {
            return this.model.destroy({ where: { id } });
        }

        return null;
    }

    restorePostById(id) {
        return this.model.restore({ where: { id } });
    }
}

export default new PostRepository(PostModel);
