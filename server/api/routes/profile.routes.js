import { Router } from 'express';
import * as userService from '../services/user.service';

const router = Router();

router
    .post('/', (req, res, next) => userService.updateUser(req.body)
        .then(data => res.send(data))
        .catch(next));

export default router;
