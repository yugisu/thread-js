import { Router } from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
        .then(comment => res.send(comment))
        .catch(next))
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(comment => res.send(comment))
        .catch(next))
    .put('/:id', (req, res, next) => commentService.update(req.params.id, req.body)
        .then(comment => res.send(comment))
        .catch(next))
    .delete('/:id', (req, res, next) => commentService.deleteCommentById(req.params.id, req.user.id)
        .then(status => res.send({ deleted: status }))
        .catch(next))
    .get('/:id/restore', (req, res, next) => commentService.restoreCommentById(req.params.id)
        .then(status => res.send({ restored: status }))
        .catch(next));

export default router;
