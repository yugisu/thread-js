import authRoutes from './auth.routes';
import postRoutes from './post.routes';
import commentRoutes from './comment.routes';
import profileRoutes from './profile.routes';
import imageRoutes from './image.routes';
// register all routes
export default (app) => {
    app.use('/api/auth', authRoutes);
    app.use('/api/posts', postRoutes);
    app.use('/api/comments', commentRoutes);
    app.use('/api/profile', profileRoutes);
    app.use('/api/images', imageRoutes);
};
