import commentRepository from '../../data/repositories/comment.repository';

export const create = (userId, comment) => commentRepository.create({
    ...comment,
    userId
});

export const update = (id, data) => commentRepository.updateById(id, data);

export const deleteCommentById = (id, userId) => commentRepository.deleteById(id, userId);

export const restoreCommentById = id => commentRepository.restoreCommentById(id);

export const getCommentById = id => commentRepository.getCommentById(id);
