import userRepository from '../../data/repositories/user.repository';

export const getUserById = async (userId) => {
    const { dataValues: { password, ...res } } = await userRepository.getUserById(userId);
    return res;
};

export const updateUser = user => userRepository.updateById(user.id, user);
